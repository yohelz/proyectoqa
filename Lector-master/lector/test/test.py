
import pytest
from lector.KindleUnpack.compatibility_utils import unicode_str,unescapeit,unquoteurl,utf8_str,quoteurl,hexlify
from urllib.parse import unquote
import html
import binascii
from datetime import datetime
from lector.KindleUnpack.mobi_utils import toHex,getLanguage,toBase32,fromBase32
from lector.KindleUnpack.mobi_sectioner import datetimefrompalmtime,describe
from lector.KindleUnpack.mobi_pagemap import int_to_roman,roman_to_int
from lector.parsers.comicbooks import is_image
from lector.KindleUnpack.unipath import pathof,fsencoding,walk,listdir

from lector.parsers.txt import *
from lector.parsers.pdf import *
from lector.rarfile.rarfile import to_datetime,is_filelike,_parse_xtime,rar3_s2k,_get_rar_version,parse_dos_time
from lector.rarfile.rarfile import load_vint, load_byte,load_le32,load_bytes
from lector.rarfile.dumprar import fmt_time,render_flags, get_file_flags
from lector.KindleUnpack.mobi_index import *
from lector.KindleUnpack.mobi_split import *

#-----------------------------------------------------#
# Aqui comienza las pruebas de compatibility_utils.py #
#-----------------------------------------------------#
def test_hexlify():
    bdata= b'Esto es una prueba'
    ej = (binascii.hexlify(bdata)).decode('ascii')
    assert hexlify(bdata) == ej


def test_es_None_utf8_str():
    a = None
    assert utf8_str(a) == None

def test_es_str_utf8_str():
    b = 'esto es un ejemplo'
    c = b.encode('utf8')
    assert utf8_str(b) == c

def test_otro_formato_utf8_str():
    d = b'\xff\xfe0\x000\x001\x001\x000\x000\x000\x000\x00 \x000\x001\x000\x000\x000\x000\x001\x000\x00'
    e = d.decode('utf16').encode('utf8')
    assert utf8_str(d, 'utf-16') == e

def test_None_unicode_str():
    a = None
    assert unicode_str(a) == None

def test_str_unicode_str():
    b = 'Esto es una prueba'
    assert unicode_str(b) == b

def test_otro_formato_unicode_str():
    d = b'\xff\xfe0\x000\x001\x001\x000\x000\x000\x000\x00 \x000\x001\x000\x000\x000\x000\x001\x000\x00'
    e = d.decode('utf-16')
    assert unicode_str(d, 'utf-16') == e


def test_quoteurl():

    href = b'<a href="https://images.app.goo.gl/aRMd1nMsmiDz38Yr7">Esto es una prueba</a>'
    resultado = '%3ca%20href%3d%22https%3a//images.app.goo.gl/aRMd1nMsmiDz38Yr7%22%3eEsto%20es%20una%20prueba%3c/a%3e'
    assert quoteurl(href) == resultado

def test_unquoteurl():
    href = '<a href="https://images.app.goo.gl/aRMd1nMsmiDz38Yr7">Esto es una prueba</a>'
    resultado = unquote(href)
    assert unquoteurl(href) == resultado

#Retorna el valor original de la variable sval
def test_unescapeit():
    #El valor original es '<html><head></head><body><h1>Esto es una prueba</h1></body></html>'
    sval = '&lt;html&gt;&lt;head&gt;&lt;/head&gt;&lt;body&gt;&lt;h1&gt;Esto es una prueba&lt;/h1&gt;&lt;/body&gt;&lt;/html&gt;'
    resultado = html.unescape(sval)
    assert unescapeit(sval) == resultado

#----------------------------------------------------#
#         Aqui comienza las pruebas de txt.py        #
#----------------------------------------------------#

#Genera y retorna los datos del txt ingresado
@pytest.mark.parametrize("parametro",["prueba.txt"])
def test_txt_generate_metadata(parametro):

    classname = ParseTXT(parametro)
    title ='prueba.txt'
    author = 'Unknown'
    year = 9999
    isbn = None
    tags = []
    cover = None
    resultado = collections.namedtuple('resultado',['title','author','year','isbn','tags','cover'])

    assert classname.generate_metadata() == resultado(title,author,year,isbn,tags,cover)

#----------------------------------------------------#
#      Aqui comienza las pruebas de rarfile.py       #
#----------------------------------------------------#

#Convierte la tupla en formato datetime
def test_to_datetime():
    t = (2020,7,22,8,50,59)
    resultado = datetime(year = 2020, month=7, day=22, hour=8, minute=50, second=59)
    assert to_datetime(t) == resultado

#La variable filename de ser un objeto
#de otro modo retornara None
def test_is_filelike():
    filename= '04 - Intratabilidad.pdf'
    resultado = False
    assert is_filelike(filename) == resultado

#No entra al primer if
#por lo que no realiza nada
#y retorna None y la posicion envianda
def test_1_parse_xtime():
    flag = 0
    data =  b'Hace mucho tiempo, en lo profundo de un bosque alejado de la civilizacion mas alla de lo alto, existe un lugar llamado paos.'
    pos = 5
    assert _parse_xtime(flag,data,pos) == (None,pos)

#entra al primer if
#se cumple el segundo if
#no entra al for
#No se cumple el trecer if
#no se cumple el cuarto if, entrando al else
def test_2_parse_xtime():
    flag = 8
    data =  b'Hace mucho tiempo, en lo profundo de un bosque alejado de la civilizacion mas alla de lo alto, existe un lugar llamado paos.'
    pos = 5
    lista = datetime(year=2032, month=3, day=3, hour=14,  minute=43, second=26)
    resultado = (lista,9)
    assert _parse_xtime(flag,data,pos) == resultado

#entra al primer if
#no se cumple el segundo if
#entra al for y le agrega a la lista microsegundos
#no entra al tercer if
#no se cumple el cuarto if, entrando al else
def test_3_parse_xtime():
    flag = 9
    data = b'Hace mucho tiempo, en lo profundo de un bosque alejado de la civilizacion mas alla de lo alto, existe un lugar llamado paos.'
    pos = 5
    lista = datetime(year=2032, month=3, day=3, hour=14, minute=43, second=26)
    listar = datetime(year=2032, month=3, day=3, hour=14, minute=43, second=26, microsecond=714342)
    resultado = (listar, 6)
    assert _parse_xtime(flag, data, pos,lista) == resultado

#Entra al primer if
#Entra en el segundo if
#Entra al for
#Entra en el tercer if
#No se cumple el cuarto if, entrando al else
def test_4_parse_xtime():
    flag = 27
    data = b'Hace mucho tiempo, en lo profundo de un bosque alejado de la civilizacion mas alla de lo alto, existe un lugar llamado paos.'
    pos = 27
    lista = datetime(year=2032, month=3, day=3, hour=14, minute=43, second=26)
    listar = datetime(year=2032, month=3, day=3, hour=14, minute=43, second=26, microsecond=769393)
    resultado = (listar, 30)
    assert _parse_xtime(flag, data, pos, lista) == resultado


#Entra al primer if
#Entra en el segundo if
#Entra al for
#No entra en el tercer if
#Si entra en el cuarto if
def test_5_parse_xtime():
    flag = 44
    data = b'Hace mucho tiempo, en lo profundo de un bosque alejado de la civilizacion mas alla de lo alto, existe un lugar llamado paos.'
    pos = 27
    lista = datetime(year=2032, month=3, day=3, hour=14, minute=53, second=48)
    listar = datetime(year=2032, month=3, day=3, hour=14, minute=53, second=49)
    resultado = (listar, 27)
    assert _parse_xtime(flag, data, pos, lista) == resultado

#Crea una contraseña apartir del nombre Vegito y
#utiliza super como clave para la decodificacion
def test_rar3_s2k():
    psw= "Vegito"
    salt = b'super'
    resultado = (b'\xcaZ\x07\xe9\x83\xff\x9a\xa2\xdbK\xb3s\nqo\xc6',b'\xb3\xb4;\xb1\xa2\x82\xdb1\x07\xe0NC\xe9|J=')
    assert rar3_s2k(psw,salt)== resultado


def test_parse_dos_time():
    tr = 1157480248
    seg,tr = tr & 0x1F, tr >> 5
    minu,tr = tr & 0x3F, tr >> 6
    hora,tr = tr & 0x1F, tr >> 5
    dia,tr = tr & 0x1F, tr >> 5
    mes,tr = tr & 0x0F, tr >> 4
    yr = (tr & 0x7F) + 1980
    seg = seg * 2
    tr2 = 1157480248
    resultado = (yr,mes,dia,hora,minu,seg)
    assert parse_dos_time(tr2) == resultado

#Cuando el archivo no es un rar
def test_1_get_rar_version():
    xfile = 'C:/Users/yohel/Desktop/Examen#2_Grupo#5.zip'
    assert _get_rar_version(xfile) == 0

#Cuando el archivo es un rar
def test_2_get_rar_version():

    xfile ='C:/Users/yohel/Desktop/Prueba.rar'
    assert _get_rar_version(xfile) == 5

def test_load_vint():
    buf = b'Esto es una prueba de funcionamiento'
    pos = 3
    resultado = (111,4)
    assert load_vint(buf,pos) == resultado


def test_load_byte():
    buf = b'Esto es una prueba de funcionamiento para esta funcion'
    pos = 8
    resultado = (117,9)
    assert load_byte(buf,pos) == resultado

def test_load_le32():
    buf = b'0x00400000'
    pos = 2
    resultado = (808726576, 6)
    assert load_le32(buf,pos) == resultado

def test_load_bytes():
    buf = b'Esto es una prueba de funcionamiento para esta funcion'
    pos = 5
    num = 1
    resultado = (b'e',6)
    assert load_bytes(buf,num,pos) == resultado



# ----------------------------------------------------#
#     Aqui comienza las pruebas de mobi_index.py      #
# ----------------------------------------------------#
def test_countSetBits():
    value = 264516164
    assert countSetBits(value) == 2

def test_getVariableWidthValue():

    data = "abcdefghijklmnopqrstvwxyzÇ"
    offset = 1
    resultado = (25,36957832160157400141023826351098755363984251440495943)
    assert getVariableWidthValue(data,offset) == resultado

#----------------------------------------------------#
#     Aqui comienza las pruebas de mobi_split.py     #
#----------------------------------------------------#

def test_getint():
    datain = b'Esto es un string de prueba'
    ofs = 0
    assert getint(datain,ofs) == 1165194351

#La funcion de writeint tiene un rango del largo permitido
#por lo que se separa en dos pruebas

#Esta es la funcion que entra en el rango
def test_dentro_writeint():
    datain = b'Esto es un string de prueba'
    ofs = 6
    n = 5
    resultado = b'Esto e\x00\x00\x00\x05 string de prueba'
    assert writeint(datain,ofs,n) == resultado

#Esta es la funcion que prueba el funcionamiento
#de la parte fuera de este rango
def test_fuera_writeint():
    datain = b'Hace mucho tiempo, en lo profundo de un bosque alejado de la civilizacion mas alla de lo alto, existe un lugar llamado paos.'
    ofs = 6
    n = 5
    resultado = b'Hace m\x00\x05ho tiempo, en lo profundo de un bosque alejado de la civiliz' b'acion mas alla de lo alto, existe un lugar llamado paos.'
    assert writeint(datain,ofs,n,78) == resultado

def test_getsecaddr():
    datain = b'Hace mucho tiempo, en lo profundo de un bosque alejado de la civilizacion mas alla de lo alto,' \
             b' existe un lugar llamado paos, este es el lugar perfecto para que una historio comience, esta historio ' \
             b'tiene como protagonista a un joven que estaria a punto de entrar en una situacion algo peligrosa, ' \
             b'pero tiene de que preocuparse este joven a sido entrenado por su abuelo desde los cinco.'
    secno = 6
    resultado = (1937007904, 1814064245)
    assert getsecaddr(datain,secno) == resultado


def test_readsection():
    datain = b'Hace mucho tiempo, en lo profundo de un bosque alejado de la civilizacion mas alla de lo alto, ' \
             b'existe un lugar llamado paos, este es el lugar perfecto para que una historio comience, esta historio ' \
             b'tiene como protagonista a un joven que estaria a punto de entrar en una situacion algo peligrosa,' \
             b' pero tiene de que preocuparse este joven a sido entrenado por su abuelo desde los cinco,'\
             b' el sabe que hacer si la situacion se torna peligrosa, su abuelo siempre le dijo que no '\
             b'se metiera en problemas pero nuestro protagonista no puedo no hacer nada, mas viendo '\
             b'la injusticia que esta pasando frente a el.'
    secno = 6
    assert readsection(datain,secno) == b''


#----------------------------------------------------#
#       Aqui comienza las pruebas de dumprar.py      #
#----------------------------------------------------#

#Cuando t es None
def test_1_fmt_time():
    t = None
    assert fmt_time(t) == '(-)'

#Cuando la variable t, cumple con la
#condicion de ser de tipo datetime
def test_2_fmt_time():
    t = datetime(year=2020, month=7, day=22, hour=8, minute=50, second=59)
    resultado = '2020-07-22T08:50:59'
    assert fmt_time(t) == resultado

#Cuando la variable t, no cumple con la
#condicion de ser de tipo datetime
def test_3_fmt_time():
    t = (2020, 7, 22, 8, 50, 59)
    resultado = '2020-07-22 08:50:59'
    assert fmt_time(t) == resultado


def test_render_flags():

    bit_list =[b'esto es una',b'prueba de funcionamiento',b'para verificar el buen',b'funcionamineto de la funcion']
    flags = 8
    resultado = 'UNK_0008'
    assert render_flags(flags,bit_list) == resultado


def test_get_file_flags():

    flags = 8
    resultado = 'COMMENT,D64'
    assert get_file_flags(flags) == resultado


#Pruebas de toten

# mobi_utils
def test_getLanguageCorrecto():
    lengprueba = getLanguage(1, 5)
    assert lengprueba == 'ar-dz'

# mobi_utils
def test_getLanguageNoID1():
    lengprueba = getLanguage(-1, 5)
    assert lengprueba == 'en'

# mobi_utils
def test_getLanguageNoID2():
    lengprueba = getLanguage(1, -1)
    assert lengprueba == 'ar'

# mobi_utils
def test_toHex():
    prueba = [133, 53, 234, 241]
    assert toHex(bytearray(prueba)) == b'8535eaf1'


def test_toBase320():
    assert toBase32(0, 1) == b'0'


def test_toBase32num():
    assert toBase32(1123, 4) == b'0133'
    assert toBase32(64, 6) == b'000020'


def test_toBase32less():
    assert toBase32(97, 0) == b'31'


def test_fromBase32Encode():
    assert fromBase32("Hola") == 616138


def test_fromBase32Empty():
    assert fromBase32(b'') == 0


def test_fromBase32Large():
    assert fromBase32(b'391758434') == 3609086660708


def test_is_imageTrue():
    assert True == is_image("Prueba.jpg")


def test_is_imageTrueUpperCase():
    assert True == is_image("Prueba.PNG")


def test_is_imageFalse():
    assert False == is_image("Prueba.txt")


def test_datetimefrompalmtimeLarge():
    fecha = datetime(year=2040, month=2, day=6, hour=6, minute=30, second=0)
    assert fecha == datetimefrompalmtime(0xFFFFFFFF + 60 + 45)


def test_datetimefrompalmtime():
    fecha = datetime(year=1970, month=1, day=2)
    assert fecha == datetimefrompalmtime(86400)


def test_describe():
    prueba = [78, 53, 128, 255]
    assert describe(bytearray(prueba)) == '"N5??" 0x4e3580ff'


def test_int_to_roman():
    assert int_to_roman(1235) == "mccxxxv"


def test_roman_to_int():
    assert roman_to_int("mmmmmmmmmmdlxxviii") == 10578

def test_pathofNone():
    assert None == pathof(None,fsencoding)

def test_pathofText():
    assert "C:/Prueba" == pathof("C:/Prueba",fsencoding)

def test_pathofBinary():
    x = "C:/Prueba"
    x = x.encode('ascii')
    assert "C:/Prueba" == pathof(x,'ascii')

def test_pathofBinaryExcept():

    assert b'\x80' == pathof(b'\x80','ascii')

def test_listdir():
    x = listdir("./CarpetaPrueba")
    assert x == ['CarpetaPrueba2', 'prueba1.txt', 'prueba2.txt', 'prueba3.txt']

def test_walk():
    x = walk("./CarpetaPrueba")
    assert x == ['prueba1.txt', 'prueba2.txt', 'prueba3.txt', 'CarpetaPrueba2\\prueba4.txt', 'CarpetaPrueba2\\prueba5.txt', 'CarpetaPrueba2\\CarpetaPrueba3\\prueba6.txt']

